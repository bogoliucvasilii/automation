package com.automation;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class ParseWithOpenCSV {
    public static void main(String[] args) {
        File file = new File("opencsv.csv");

        try {
            FileReader input = new FileReader(file);

            CSVReader reader = new CSVReader(input,';');

            List<String[]> records = reader.readAll();
            for (String[] record : records) {
                System.out.println("ID : " + record[0]);
                System.out.println("Name : " + record[1]);
                System.out.println("Mark : " + record[2]);
                System.out.println("===================");
            }


            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
