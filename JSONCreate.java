package com.automation;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class JSONCreate {
    public static void main(String[] args) {

            JSONObject company = new JSONObject();
            JSONObject department = new JSONObject();
            JSONObject employee1 = new JSONObject();
            JSONObject employee = new JSONObject();
            JSONArray skills = new JSONArray();

            skills.add(0,"Creativity");
            skills.add(1,"Strong attention to detail");
            skills.add(2,"Good communication skills");

            employee1.put("firstName","Armasula");
            employee1.put("lastName","Alecsandru");
            employee1.put("position","Marketing Director");
            employee1.put("brithDate","03.04.1995");
            employee1.put("managerId",0);

            employee1.put("skills",skills);
            employee.put("employee",employee1);
            department.put("department",employee);
            company.put("company",department);


            try (FileWriter file = new FileWriter("companytest.json")) {

                file.write(company.toJSONString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.print(company);

    }
}
