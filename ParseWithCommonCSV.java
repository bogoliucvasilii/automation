package com.automation;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;


public class ParseWithCommonCSV {
    private static final String SAMPLE_CSV_FILE = "./commoncsv.csv";

    public static void main(String[] args) {
        try (
                Reader reader = new BufferedReader(new FileReader(SAMPLE_CSV_FILE));

                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim());


        ) {
            for (CSVRecord csvRecord : csvParser) {
                // Accessing values by Header names
                String id = csvRecord.get("ID");
                String name = csvRecord.get("Name");
                String marks = csvRecord.get("Marks");


                System.out.println("Record No - " + csvRecord.getRecordNumber());
                System.out.println("---------------");
                System.out.println("ID : " + id);
                System.out.println("Name : " + name);
                System.out.println("Mark : " + marks);
                System.out.println("---------------\n\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
