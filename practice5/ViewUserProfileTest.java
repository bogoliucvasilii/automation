package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Menu;
import com.alliedtesting.poms.People;
import org.testng.annotations.Test;

public class ViewUserProfileTest extends  BaseTest {
    @Test
    public void  test(){
        String username = "actemell";
        loginWithDefaultUser();

        Menu menu = new Menu(driver);
        if(!menu.peopleLnk.isDisplayed()) {
            menu.manageLnk.click();
        }
        menu.peopleLnk.click();
        menu.waitTitleToBe("People",10);
        menu.gotoUserProfile(username);
        menu.waitTitleToBe(username,10);
    }
}

