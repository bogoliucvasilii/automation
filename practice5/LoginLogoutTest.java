package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Menu;
import org.testng.annotations.Test;

public class LoginLogoutTest extends BaseTest {
    @Test
    public void loginLogoutTest(){
        loginWithUser("actemell", "A068199787a");

        Menu menu = new Menu(driver);
        menu.logoutLnk.click();
        menu.checkIfLoggedOut(10);
    }
}
