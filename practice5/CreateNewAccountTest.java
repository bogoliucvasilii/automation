package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.CreateNewAccount;
import com.alliedtesting.poms.Menu;
import org.testng.annotations.Test;

public class CreateNewAccountTest extends BaseTest {
    @Test
    public void test(){
        Menu menu = new Menu(driver);
        menu.loginLnk.click();

        menu.createAccountLnk.click();
        String email  = "newacc@mail.ru";
        String uname = "newacc";
        String timezone = "Gaza";
        CreateNewAccount newAccount = new CreateNewAccount(driver);
        newAccount.emailInput.sendKeys(email);
        newAccount.userNameInput.sendKeys(uname);
        newAccount.personalInfoChkBox.click();
        newAccount.timeZoneSelect.selectByVisibleText(timezone);
        newAccount.createAccountBtn.click();

        newAccount.checkAccountCreated();


    }
}
