package com.alliedtesting.poms;

import com.alliedtesting.controls.*;
import com.alliedtesting.core.AbstractPOM;
import com.alliedtesting.core.Helpers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import javax.swing.*;

public class CreateNewAccount extends AbstractPOM {

    public CreateNewAccount(WebDriver driver) { super(driver);}

    @FindBy(xpath = "//input[@name='mail']")
    public WebTextInput emailInput;

    @FindBy(xpath = "//input[@name='name']")
    public WebTextInput userNameInput;

    @FindBy(xpath = "//input[@type='checkbox']")
    public WebCheckBox personalInfoChkBox;

    @FindBy(xpath = "//select[contains(@class,'timezone')]")
    public WebSelect timeZoneSelect;

    @FindBy(xpath = "//input[@type='submit' and @name='op']")
    public WebButton createAccountBtn;

    @FindBy(xpath = "//div[@role='contentinfo']")
    public WebText infoMessage;

    public void checkAccountCreated()
    {
        log.info("Checking if account created");
        String message = "Thank you for applying for an account.";
        Helpers.check2StringIfEquals(infoMessage.getText(),message);
    }


}
