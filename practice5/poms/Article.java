package com.alliedtesting.poms;

import com.alliedtesting.controls.WebButton;
import com.alliedtesting.controls.WebText;
import com.alliedtesting.controls.WebTextInput;
import com.alliedtesting.core.AbstractPOM;
import com.alliedtesting.core.Helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class  Article extends AbstractPOM {

    public Article(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath="//div[@role='contentinfo']")
    public WebText infoMessage;

    @FindBy(xpath="//article/div[contains(@class,'node__content')]/div")
    public WebText contentTxt;

    @FindBy(xpath = "//input[@value='Save']")
    public WebButton saveBtn;


    public void checkCreatedArticleMessage(String articleName){
        String message = "Article " + articleName + " has been created.";
        Helpers.check2StringIfEquals(infoMessage.getText(), message);
    }

    public void checkDeletedArticleMessage(String articleName){
        String message = "The Article " + articleName + " has been deleted.";
        Helpers.check2StringIfEquals(infoMessage.getText(), message);
    }
}
