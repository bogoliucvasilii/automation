package com.alliedtesting.poms;

import com.alliedtesting.controls.WebLink;
import com.alliedtesting.controls.WebText;
import com.alliedtesting.core.AbstractPOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.Link;

public class Menu extends AbstractPOM {

    public Menu(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//h1[contains(@class,'page-title')]")
    public WebText pageTitle;

    @FindBy(xpath = "//ul[@class='clearfix menu']/li/a[text()='Log out']")
    public WebLink logoutLnk;

    @FindBy(xpath = "//div[@id='toolbar-administration']//a[text()='Content']")
    public WebLink contentLnk;

    @FindBy(xpath = "//div[@id='toolbar-administration']//a[text()='Manage']")
    public WebLink manageLnk;

    @FindBy(xpath = "//div[@class='toolbar-menu-administration']//a[text()='People']")
    public WebLink peopleLnk;

    @FindBy(xpath = "//*[@id=\"block-bartik-account-menu\"]/div/ul/li/a")
    public WebLink loginLnk;

    @FindBy(xpath = "//a[@href='/user/register']")
    public WebLink createAccountLnk;

    @FindBy(xpath = "//div[@role='contentinfo']")
    public WebText messageBox;

    public void waitTitleToBe(String value, int seconds){
        log.info("Check that page title is '" + value + "' in '" + seconds + "' seconds");
        (new WebDriverWait(driver, seconds)).until(ExpectedConditions.textToBePresentInElement(pageTitle, value));
    }

    public void checkIfLoggedOut(int seconds) {
        log.info("Checking if user logged out in " + seconds + " seconds");
        (new WebDriverWait(driver,seconds)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"block-bartik-account-menu\"]/div/ul/li/a")));
    }

    public void gotoUserProfile(String uname) {
        log.info("Going to " + uname + " profile");
        String xpath = "//a[@class = 'username' and text()='" + uname + "']";
        WebElement ulink = driver.findElement(By.xpath(xpath));
        ulink.click();
    }

}
