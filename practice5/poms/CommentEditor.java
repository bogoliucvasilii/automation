package com.alliedtesting.poms;

import com.alliedtesting.controls.WebButton;
import com.alliedtesting.controls.WebText;
import com.alliedtesting.controls.WebTextInput;
import com.alliedtesting.core.AbstractPOM;
import com.alliedtesting.core.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CommentEditor extends AbstractPOM {

    public CommentEditor(WebDriver driver) { super(driver); }


    @FindBy(xpath = "//input[@id='edit-subject-0-value']")
    public WebTextInput commentTitle;

    @FindBy(xpath = "//body")
    WebElement contentInput;


    @FindBy(xpath = "//input[@value='Save']")
    public WebButton saveBtn;

    @FindBy(xpath="//div[@role='contentinfo']")
    public WebText infoMessage;

    public void fillInContent(String value){
        WebElement commentFrame = driver.findElement(By.xpath("//iframe[contains(@title,'Comment field')]"));
        driver.switchTo().frame(commentFrame);
        contentInput.clear();
        contentInput.sendKeys(value);
        driver.switchTo().defaultContent();
    }


    public void changeCommentTitle(String newTitle){
        commentTitle.clear();
        commentTitle.sendKeys(newTitle);
    }

    public void changeCommentText(String newText){
        fillInContent(newText);
    }

    public void checkCommentEdited(){
        String message = "Your comment has been posted.";
        Helpers.check2StringIfEquals(infoMessage.getText(), message);
    }

}
