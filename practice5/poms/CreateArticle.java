package com.alliedtesting.poms;

import com.alliedtesting.controls.*;
import com.alliedtesting.core.AbstractPOM;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateArticle extends AbstractPOM {

    public CreateArticle(WebDriver driver){
        super(driver);
    }


    @FindBy(xpath = "//div[label[contains(text(),'Title')]]/input[contains(@id,'edit-title')]")
    public WebTextInput titleInput;

    @FindBy(xpath = "//body")
    private WebElement contentInput;

    public void fillInContent(String value){
        driver.switchTo().frame(0);
        contentInput.sendKeys(value);
        driver.switchTo().defaultContent();
    }

    @FindBy(xpath = "//input[@value='Save']")
    public WebButton saveBtn;

}
