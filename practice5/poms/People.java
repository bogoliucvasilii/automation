package com.alliedtesting.poms;

import com.alliedtesting.controls.*;
import com.alliedtesting.core.AbstractPOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class People extends AbstractPOM {

    public People(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[text()='Add user']")
    public WebButton addUserBtn;

    @FindBy(xpath = "//a[@class='username']")
    public WebLink userName;

    @FindBy(xpath = "//*[@id=\"block-seven-primary-local-tasks\"]/nav/nav/ul/li[3]/a")
    public WebLink rolesLnk;

    @FindBy(xpath = "//*[@id=\"block-seven-local-actions\"]/ul/li/a")
    public WebLink addRoleLnk;

    @FindBy(xpath = "//input[@class = 'form-text required machine-name-source']")
    public WebTextInput roleNameInput;

    @FindBy(xpath = "//input[@class = 'form-text required machine-name-target']")
    public WebTextInput roleMachineReadableName;

    @FindBy(xpath = "//input[@value = 'Save']")
    public WebButton saveBtn;


    public void checkRolePresence(String roleName) {
        log.info("Checking role presence");
        String xpath = "//table[@id='edit-entities']//tr[@data-drupal-selector='edit-entities-" + roleName + "']";
        if(driver.findElement(By.xpath(xpath)).isDisplayed()) {
            log.info("Role is created");
        }
        else {
            log.info("Role isn't created");
        }
    }


}
