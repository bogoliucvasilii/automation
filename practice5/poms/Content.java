package com.alliedtesting.poms;

import com.alliedtesting.controls.WebButton;
import com.alliedtesting.core.AbstractPOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class Content extends AbstractPOM {

    public Content(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//main//a[text()='Add content']")
    public WebButton addContentBtn;

    public void deleteArticleByName(String artName){
        log.info("Opening " + artName + " article");
        String xpath = "//a[text() = '" + artName + "']";
        driver.findElement(By.xpath(xpath)).click();
        driver.findElement(By.xpath("//a[text()='Delete']")).click();
        driver.findElement(By.xpath("//input[@value='Delete']")).click();
        log.info("Article " + artName + " has been deleted");
    }
}
