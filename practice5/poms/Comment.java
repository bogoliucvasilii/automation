package com.alliedtesting.poms;

import com.alliedtesting.controls.WebButton;
import com.alliedtesting.controls.WebText;
import com.alliedtesting.controls.WebTextInput;
import com.alliedtesting.core.AbstractPOM;
import com.alliedtesting.core.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Comment extends AbstractPOM {

    public Comment(WebDriver driver) { super(driver); }

    @FindBy(xpath = "//input[@type='text']")
    public WebTextInput commentTitle;

    @FindBy(xpath = "//body")
    private WebElement contentInput;

    @FindBy(xpath = "//input[@value='Save']")
    public WebButton saveBtn;

    @FindBy(xpath="//div[@role='contentinfo']")
    public WebText infoMessage;



    public void fillInContent(String value){
        WebElement commentFrame = driver.findElement(By.xpath("//iframe[contains(@title,'Comment field')]"));
        driver.switchTo().frame(commentFrame);
        contentInput.sendKeys(value);
        driver.switchTo().defaultContent();
    }

    public void checkCreatedComment(){
        String message = "Your comment has been posted.";
        Helpers.check2StringIfEquals(infoMessage.getText(), message);
    }

    public void addCommentOnArticle(String article, String commentTitle, String commentBody) {

        String xpath = "//a[text()='"+article+"']";
        log.info("Going to "+article+" article page");
        driver.findElement(By.xpath(xpath)).click();

        log.info("Entering comment subject");
        this.commentTitle.sendKeys(commentTitle);
        log.info("Entering comment text");
        fillInContent(commentBody);
        log.info("Saving comment");
        saveBtn.click();
        log.info("Checking if comment is created");
        checkCreatedComment();
    }

    public void gotoArticle(String articleName){
        log.info("Going to article "+ articleName);
        String xpath = "//a[contains(text(),'"+articleName+"')]";
        driver.findElement(By.xpath(xpath)).click();
    }

    public void findCommentByName(String commentName) {
        log.info("Finding comment with '"+ commentName + "' name");
        String xpath = "//ul[preceding-sibling::h3[./a[text()='" + commentName + "']]]//li//a[text()='Edit']";
        driver.findElement(By.xpath(xpath)).click();
        log.info("Comment found ");
    }

}
