package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Comment;
import com.alliedtesting.poms.CommentEditor;
import com.alliedtesting.poms.Menu;
import org.testng.annotations.Test;

public class EditCommentTest extends BaseTest {
    @Test
    public void test(){
        loginWithDefaultUser();
        Menu menu = new Menu(driver);
        if(!menu.contentLnk.isDisplayed()) {
            menu.manageLnk.click();
        }
        menu.contentLnk.click();

        String articleName = "Some article";
        String commentName = "Another comment";
        Comment comment = new Comment(driver);
        comment.gotoArticle(articleName);
        comment.findCommentByName(commentName);


        String newTitle = "Another comment New title";
        String newCommentBody = "New body for anthoer comment";
        CommentEditor commentEditor = new CommentEditor(driver);
        commentEditor.changeCommentTitle(newTitle);
        commentEditor.changeCommentText(newCommentBody);
        commentEditor.saveBtn.click();
        commentEditor.checkCommentEdited();
    }
}
