package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Article;
import com.alliedtesting.poms.Content;
import com.alliedtesting.poms.Menu;
import org.testng.annotations.Test;

public class DeleteArticleTest extends BaseTest {
    @Test
    public void test(){
        loginWithDefaultUser();
        Menu menu = new Menu(driver);
        if(!menu.contentLnk.isDisplayed()) {
            menu.manageLnk.click();
        }
        menu.contentLnk.click();
        menu.waitTitleToBe("Content",10);

        String articleStr = "newa rt";
        Content content = new Content(driver);
        content.deleteArticleByName(articleStr);

        Article article = new Article(driver);
        article.checkDeletedArticleMessage(articleStr);

    }
}
