package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.*;
import org.testng.annotations.Test;

public class CreateNewArticleTest extends BaseTest {
    @Test
    public void test(){
        loginWithDefaultUser();
        Menu menu = new Menu(driver);

        if(!menu.contentLnk.isDisplayed()) {
            menu.manageLnk.click();
        }
        menu.contentLnk.click();

        menu.waitTitleToBe("Content", 10);
        Content content = new Content(driver);
        content.addContentBtn.click();

        menu.waitTitleToBe("Add content", 10);
        AddContent addContent = new AddContent(driver);
        addContent.articleLnk.click();

        menu.waitTitleToBe("Create Article", 10);
        CreateArticle createArticle = new CreateArticle(driver);
        createArticle.titleInput.sendKeys("test article");
        createArticle.fillInContent("content to check");
        createArticle.saveBtn.click();


        Article article = new Article(driver);
        article.checkCreatedArticleMessage("test article");
        article.contentTxt.checkIfEquals("content to check");

    }
}
