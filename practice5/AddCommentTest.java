package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Comment;
import com.alliedtesting.poms.Menu;
import org.testng.annotations.Test;



public class AddCommentTest extends BaseTest {
    @Test
    public void test(){
        loginWithDefaultUser();

        Menu menu = new Menu(driver);
        if(!menu.contentLnk.isDisplayed()) {
            menu.manageLnk.click();
        }
        menu.contentLnk.click();

        Comment comment = new Comment(driver);

        comment.addCommentOnArticle("Some article","Another comment", "Another random comment");


    }

}
