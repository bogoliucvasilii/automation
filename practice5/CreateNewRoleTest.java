package com.alliedtesting.tests;

import com.alliedtesting.core.BaseTest;
import com.alliedtesting.poms.Menu;
import com.alliedtesting.poms.People;
import org.testng.annotations.Test;

public class CreateNewRoleTest extends BaseTest {
    @Test
        public void test() throws InterruptedException {
            loginWithDefaultUser();
            Menu menu = new Menu(driver);
            if(!menu.peopleLnk.isDisplayed()) {
                menu.manageLnk.click();
            }
            menu.peopleLnk.click();

            String roleName = "hadsflgudfgi";
            log.info("Creating new role with " + roleName + " name");
            People people = new People(driver);
            people.rolesLnk.click();
            people.addRoleLnk.click();
            people.roleNameInput.sendKeys(roleName);
            Thread.sleep(1000);
            //people.roleMachineReadableName.sendKeys(roleName);
            people.saveBtn.click();
            people.checkRolePresence(roleName);
    }

}
