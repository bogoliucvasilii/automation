Feature: Log in web-site

  Scenario Outline: Logging in web-site
    Given link "http://dev-vasilii.pantheonsite.io/"
    When I open link
    And Click on Log in link
    And Fill "<username>" and "<password>" field
    And Click on Log in button
    Then Page title should be "<username>"


    Examples:
      | username | password    |
      | actemell | A068199787a |