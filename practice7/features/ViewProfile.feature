Feature: Viewing user profile

  Scenario Outline:
    Given link "http://dev-vasilii.pantheonsite.io/"
    When I open link
    And Click on Log in link
    And Fill "<username>" and "<password>" field
    And Click on Log in button
    And Click on Manage Link
    And Click on People link
    And Click on "<user>" in users table
    Then New page is opened with page title "<user>"

    Examples:
      | username | password    | user |
      | actemell | A068199787a | newacc|