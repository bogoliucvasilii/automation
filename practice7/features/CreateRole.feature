Feature: Creating new role

Scenario Outline:
  Given link "http://dev-vasilii.pantheonsite.io/"
  When I open link
  And Click on Log in link
  And Fill "<username>" and "<password>" field
  And Click on Log in button
  And Click on Manage Link
  And Click on People link
  And Click on Roles link
  And Click on Add role button
  And Fill "Role name" Field with "<role>"
  And Click on Save button
  Then Message "Role <role> has been added" should appear


Examples:
| username | password    | role           |
| actemell | A068199787a | newrole123     |