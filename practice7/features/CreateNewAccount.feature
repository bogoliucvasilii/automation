Feature: Creating new account

Scenario Outline:
  Given link "http://dev-vasilii.pantheonsite.io/"
  When I open link
  And Click on Log in link
  And Click on Create new account link
  And Fill "<email>" and "<username>" in fields
  And Click on Create new account button
  Then Message "Thank you for applying for an account." should appear in top


Examples:
| username | email    |
| fresh_user | newemail321@mail.ru |