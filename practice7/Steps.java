package cucumber;

import bsh.StringUtil;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import org.apache.commons.lang3.StringUtils;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class Steps {
    public   WebDriver driver = new ChromeDriver();
    public  String url;
    public  String loginLnk = "//a[@href='/user/login']";
    public  String usernameField = "//input[@id='edit-name']";
    public  String passwordField = "//input[@id='edit-pass']";
    public String loginButton = "//form[@id='user-login-form']//input[@id='edit-submit']";
    public String pageTitle = "//h1[contains(@class,'title')]";
    public String username;

    @Given("link {string}")
    public void link_http_dev_vasilii_pantheonsite_io(String string) {
        url = string;
    }

    @When("I open link")
    public void i_open_link() {
        driver.get(url);
    }

    @When("Click on Log in link")
    public void click_on_Log_in_link() {
        driver.findElement(By.xpath(loginLnk)).click();
    }

    @When("Fill {string} and {string} field")
    public void fill_and_field(String string, String string2) {
        username = string;
        driver.findElement(By.xpath(usernameField)).sendKeys(string);
        driver.findElement(By.xpath(passwordField)).sendKeys(string2);

    }

    @When("Click on Log in button")
    public void click_on_Log_in_button() {
        driver.findElement(By.xpath(loginButton)).click();
    }

    @Then("Page title should be {string}")
    public void page_title_should_be(String string) {
        String pagetitle = driver.findElement(By.xpath(pageTitle)).getText();

        if(StringUtils.containsIgnoreCase(username,pagetitle))
            System.out.println("Test sucessful");

        driver.close();
        driver.quit();

    }


    //Everything else////---------------------------------------
    //Create role steps

    String peopleLnk = "//a[@href='/admin/people']";
    String roleLnk = "//a[@href='/admin/people/roles']";
    String roleBtn = "//a[@class='button button-action button--primary button--small']";
    String roleNameInput = "//input[@id='edit-label']";
    String saveRoleBtn = "//input[@id='edit-submit']";
    String manageLnk = "//a[@href='/admin']";

    @When("Click on Manage Link")
    public void click_on_manage_link(){
        driver.findElement(By.xpath(manageLnk)).click();
    }

    @When("Click on People link")
    public void click_on_People_link() {

        driver.findElement(By.xpath(peopleLnk)).click();
    }
    @When("Click on Roles link")
    public void click_on_Roles_link() {

        driver.findElement(By.xpath(roleLnk)).click();
    }
    @When("Click on Add role button")
    public void click_on_Add_role_button() {

        driver.findElement(By.xpath(roleBtn)).click();
    }
    @When("Fill {string} Field with {string}")
    public void fill_Field_with(String string, String string2) throws InterruptedException {

        driver.findElement(By.xpath(roleNameInput)).sendKeys(string2);
        Thread.sleep(1000);
    }
    @When("Click on Save button")
    public void click_on_save_button() {
        driver.findElement(By.xpath(saveRoleBtn)).click();
    }
    @Then("Message {string} should appear")
    public void message_should_appear(String string) {
        if(StringUtils.containsIgnoreCase(driver.findElement(By.xpath(infoMessage)).getText(),"has been added"))
        System.out.println("Role creation successful");

    }


    ////////Create new account-------------------------------
    String creatAccountLink = "//a[contains(text(),'Create new account')]";
    String email = "//input[@id='edit-mail']";
    String uname = "//input[@id='edit-name']";
    String createAccountBtn = "//form[@id='user-register-form']//input[@id='edit-submit']";
    String infoMessage = "//div[@role='contentinfo']";

    @When("Click on Create new account link")
    public void click_on_Create_new_account_link() {
        driver.findElement(By.xpath(creatAccountLink)).click();
    }
    @When("Fill {string} and {string} in fields")
    public void fill_and_fields(String string, String string2) {
        driver.findElement(By.xpath(email)).sendKeys(string);
        driver.findElement(By.xpath(uname)).sendKeys(string2);
    }
    @When("Click on Create new account button")
    public void click_on_Create_new_account_button() {
            driver.findElement(By.xpath(createAccountBtn)).click();
    }
    @Then("Message {string} should appear in top")
    public void message_should_appear_in_top(String string) {
        if(StringUtils.containsIgnoreCase(driver.findElement(By.xpath(infoMessage)).getText(),"Thank you for applying for an account"))
        System.out.println("Account creation sucessful");
        driver.close();
    }

    ////View Profile Steps-----------------------------
    String user = "";
    String userTitle = "";

    @When("Click on {string} in users table")
    public void click_on_in_users_table(String string) {
        user += "//a[contains(text(),'" + string + "')]";
        driver.findElement(By.xpath(user)).click();


    }
    @Then("New page is opened with page title {string}")
    public void new_page_is_opened_with_page_title(String string) {
        userTitle += "//h1[text()='" + string + "']";
        if(StringUtils.containsIgnoreCase(driver.findElement(By.xpath(userTitle)).getText(),string))
            System.out.println("Title contains users " + string + " name");

    }


}
