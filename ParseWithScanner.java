package com.automation;

import java.io.*;

public class ParseWithScanner {
    public static void main(String[] args) {
        String path = "opencsv.csv";
        String line = "";
        String separator = ";";

        try {

            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {


                String[] students = line.split(separator);

                System.out.println("Student [ID= " + students[0] + " , Name=" + students[1] + " , Mark=" + students[2] + "]");


            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}