package com.automation;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateCSVWithOpenCSV {
    public static void main(String[] args) {
        File file = new File("opencsv.csv");

        try {
            FileWriter outputfile = new FileWriter(file);

            CSVWriter writer = new CSVWriter(outputfile, ';',
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);

            List<String[]> data = new ArrayList<String[]>();
            data.add(new String[] { "1", "Bogoliuc Vasilii", "10" });
            data.add(new String[] { "2", "Bulat Ilie", "20" });
            data.add(new String[] { "3", "Cara Stefan", "30"});
            data.add(new String[] { "4", "Croitor Florin", "40"});
            data.add(new String[] { "5", "Dilion Mihai", "50"});
            data.add(new String[] { "6", "Armasula Alexandru", "60"});

            writer.writeAll(data);

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
