package com.automation;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.util.Arrays;

public class CreateCSVWithCommonCSV {
    private static final String SAMPLE_CSV_FILE = "./commoncsv.csv";

    public static void main(String[] args) throws IOException {
        try (
                Writer writer = new BufferedWriter(new FileWriter(SAMPLE_CSV_FILE));

                CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                        .withHeader("ID", "Name", "Marks"));
        ) {
            csvPrinter.printRecord("1", "Bogoliuc Vasilii", "10");
            csvPrinter.printRecord("2", "Bulat Ilie", "20");
            csvPrinter.printRecord("3", "Cara Stefan", "30");
            csvPrinter.printRecord("4", "Croitor Florin", "40");
            csvPrinter.printRecord("5", "Dilion Mihai", "50");
            csvPrinter.printRecord("6", "Armasula Alexandru", "60");

            csvPrinter.flush();
        }
    }
}
