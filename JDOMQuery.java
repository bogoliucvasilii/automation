package com.automation;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JDOMQuery {
    public static void main(String[] args) {

        try {
            File inputFile = new File("company.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            String requestedEmpId = "05";

            System.out.println("Root element :" + document.getRootElement().getName());
            Element company = document.getRootElement();
            System.out.println("----------------------------");

            List<Element> departmentsList = company.getChildren();
            for(int  i = 0; i < departmentsList.size(); i++)
            {
                Element department = departmentsList.get(i);
                List<Element> employeeList = department.getChildren();
                for(int j = 0; j < employeeList.size(); j++)
                {
                    Element employee = employeeList.get(j);
                    Attribute attribute = employee.getAttribute("empId");
                    if(requestedEmpId.equalsIgnoreCase(attribute.getValue())) {
                        Attribute attrId  = department.getAttribute("depId");
                        Attribute attrName = department.getAttribute("name");


                        System.out.println("Department name :" + attrName.getValue() + "\nDepartment ID :" + attrId.getValue());
                        System.out.println("----------------------------");
                        System.out.println("employee ID : " + attribute.getValue());
                        System.out.println("First Name : " + employee.getChild("firstName").getText());
                        System.out.println("Last Name : " + employee.getChild("lastName").getText());
                        System.out.println("Birth date : " + employee.getChild("birthDate").getText());
                        System.out.println("Position : " + employee.getChild("position").getText());
                        System.out.println("Manager ID : " + employee.getChild("managerId").getText());

                        System.out.println("Skills : ");
                        List<Element> skillsList = employee.getChildren("skills").get(0).getChildren();
                        for(Element skill:skillsList)
                        {
                            System.out.println(skill.getText());
                        }
                    }

                }
            }
            System.out.println("----------------------------");
            } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
