package com.automation;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

public class StAXParser {


    public static void main(String[] args)
    {
        boolean bFirstName = false;
        boolean bLastName = false;
        boolean bPosition = false;
        boolean bManagerId = false;
        boolean bSkill = false;
        boolean bBirthDate = false;
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader("company.xml"));

            while(eventReader.hasNext())
            {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType())
                {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        if(qName.equalsIgnoreCase("department"))
                        {
                            System.out.println("Start Element: department");

                                String name = attributes.next().getValue();
                                System.out.println("Department name : " + name);
                                String id = attributes.next().getValue();
                                System.out.println("Department id : " + id);
                        }

                        else if(qName.equalsIgnoreCase("employee"))
                        {
                            System.out.println("\nCurrent Element: employee");
                            String empid = attributes.next().getValue();
                            System.out.println("Employee ID : " + empid);
                        }
                        else if(qName.equalsIgnoreCase("firstName"))
                            bFirstName=true;
                        else if(qName.equalsIgnoreCase("lastName"))
                            bLastName=true;
                        else if(qName.equalsIgnoreCase("position"))
                            bPosition=true;
                        else if(qName.equalsIgnoreCase("managerId"))
                            bManagerId = true;
                        else if(qName.equalsIgnoreCase("skill"))
                            bSkill = true;
                        else if(qName.equalsIgnoreCase("birthDate"))
                            bBirthDate = true;
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if(bFirstName) {
                            System.out.println("First name : " + characters.getData());
                            bFirstName = false;
                        }
                        if(bLastName){
                            System.out.println("Last name : " + characters.getData());
                            bLastName=false;
                            }
                        if(bPosition) {
                            System.out.println("Position : " + characters.getData());
                            bPosition = false;
                        }
                        if(bManagerId) {
                            System.out.println("Manager Id : " + characters.getData());
                            bManagerId = false;
                        }
                        if(bSkill)
                        {
                            System.out.println("Skill : " + characters.getData());
                            bSkill = false;
                        }
                        if(bBirthDate)
                        {
                            System.out.println("Birth Date : " + characters.getData());
                            bBirthDate = false;
                        }
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();

                        if(endElement.getName().getLocalPart().equalsIgnoreCase("department")) {
                            System.out.println("End Element : department");
                            System.out.println();
                        }
                        break;
                }
            }


        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
